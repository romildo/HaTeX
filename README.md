
# The HaTeX library

HaTeX is a Haskell library that implements the *LaTeX syntax*, plus some abstractions on top.

Check a list of usage examples in the [Examples](https://gitlab.com/daniel-casanueva/haskell/HaTeX/-/tree/main/Examples) directory
of the repository in GitHub.
A good starting point is [simple.hs](https://gitlab.com/daniel-casanueva/haskell/HaTeX/-/blob/main/Examples/simple.hs?ref_type=heads).
Run any example script executing the `main` function.

## How to contribute

There are the options for potential contributors:

* Fork the [GitLab repository](https://gitlab.com/daniel-casanueva/haskell/HaTeX) and open a merge request.
* Report bugs or make suggestions opening a ticket in the [Issue Tracker](https://gitlab.com/daniel-casanueva/haskell/HaTeX/-/issues).

## Related projects

* [haskintex](https://daniel-casanueva.gitlab.io/haskell/haskintex): Tool to use Haskell (and, additionaly, the HaTeX library)
within a LaTeX file.
* [TeX-my-math](https://github.com/leftaroundabout/Symbolic-math-HaTeX): Experimental library to ease the production
of mathematical expressions using HaTeX.
